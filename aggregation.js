db.fruits.insertMany([
{
	name: "Apple",
	color: "Red",
	stock: 20,
	price: 40,
	supplier_id: 1,
	onSale: true,
	origin: ["Philippines", "USA"]
},

{
	name: "Banana",
	color: "Yellow",
	stock: 15,
	price: 20,
	supplier_id: 2,
	onSale: true,
	origin: ["Philippines", "Equador"]
},

{
	name: "Kiwi",
	color: "Green",
	stock: 25,
	price: 50,
	supplier_id: 3,
	onSale: true,
	origin: ["USA", "China"]
},

{
	name: "Mango",
	color: "Yellow",
	stock: 10,
	price: 120,
	supplier_id: 4,
	onSale: false,
	origin: ["Philippines", "India"]
}

]);


db.fruits.find();



//[SECTION] - MongoDB Aggregation

/*

Used 


Syntax:

*/ 

db.fruits.aggregate([
{ $match: {onSale: true} },    // match is used to pass the documents that meet the specified condition/s to the next aggreagation stage.

{ $group: { _id: "$supplier_id", total: {$sum: "$stock"} } }    //group is used to group elements together and field-value

]);


// Field projection with aggregation

db.fruits.aggregate([
	{
		$match: { onSale: true} },
		{$group : { _id: "$supplier_id", total: { $sum: "$stock"} } },
		{$project: { _id: 0}}
]);

// Sorting aggregated results
/*

Syntax:

1 -ascending
-1 - descending


*/

db.fruits.aggregate([
	{
		$match: { onSale: true} },
		{$group : { _id: "$supplier_id", total: { $sum: "$stock"} } },
		{$sort: { total: -1}}
]);


//Aggregating results based on array fields

/*

- $unwind - demonstrate an array field from a collection with an array value to output a result for aech element

Syntax:
{ $unwind: field}

*/ 

db.fruits.aggregate ([
	{ $unwind: "$origin" }
]);


db.fruits.aggregate ([
	{ $unwind: "$origin" },
	{ $group: { _id: "$origin", kinds: {$sum: 1} } }
]);

//Other Aggregation stages

// $count

db.fruits.aggregate ([
	{ $match: {color: "Yellow"}},
	{ $count: "Yellow Fruits"}
]);


// $avg
db.fruits.aggregate ([
	{ $match: {color: "Yellow"}},
	{ $group: {_id: "$color", yellow_fruits_stock: {$avg: "$stock"} } }
]);

// $min & $max
db.fruits.aggregate ([
	{ $match: {color: "Yellow"}},
	{ $group: {_id: "$color", yellow_fruits_stock: {$min: "$stock"} } }
]);

db.fruits.aggregate ([
	{ $match: {color: "Yellow"}},
	{ $group: {_id: "$color", yellow_fruits_stock: {$max: "$stock"} } }
]);